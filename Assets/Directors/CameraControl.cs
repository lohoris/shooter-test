﻿using UnityEngine;

public class CameraControl : MonoBehaviour
{
	public bool mouseLook
	{
		get
		{
			return mouseSpeedH > 0 || mouseSpeedV > 0;
		}
	}

	public float mouseSpeedH = 0;
	public float mouseSpeedV = 0;
}
