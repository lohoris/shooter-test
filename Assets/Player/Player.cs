﻿using UnityEngine;

public class Player : MonoBehaviour
{
	[SerializeField] private KeyCode cameraKey = KeyCode.None;
	[SerializeField] private CameraControl[] cameras = null;

	private int currentCameraIdx;
	private CameraControl currentCamera
	{
		get
		{
			return cameras[currentCameraIdx];
		}
	}

	private void Start ()
	{
		Camera.main.transform.SetParent( this.transform );
		SetCamera( 0 );
	}
	private void Update ()
	{
		if ( Input.GetKeyDown( this.cameraKey ) )
		{
			this.CycleCamera();
		}

		if ( currentCamera.mouseLook )
		{
			float mh = Input.GetAxis( "Horizontal" ) * Time.deltaTime;
			float mv = Input.GetAxis( "Vertical" ) * Time.deltaTime;

			this.transform.localRotation *= Quaternion.Euler( 0, mh * currentCamera.mouseSpeedH, 0 );

			Camera.main.transform.localRotation *= Quaternion.Euler( mv * currentCamera.mouseSpeedV, 0, 0 );
		}
	}

	private void CycleCamera ()
	{
		SetCamera( currentCameraIdx + 1 );
	}
	private void SetCamera ( int setIdx )
	{
		if ( setIdx >= cameras.Length )
		{
			setIdx = 0;
		}
		else if ( setIdx < 0 )
		{
			setIdx = cameras.Length - 1;
		}

		// TODO: smooth transition instead

		currentCameraIdx = setIdx;
		Camera.main.transform.position = currentCamera.transform.position;
		Camera.main.transform.rotation= currentCamera.transform.rotation;
	}
}
